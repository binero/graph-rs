extern crate collections_rs as collections;

mod map;

pub use map::DirectedMapGraph;

/// A directed graph with edge weights. The efficiency of the different
/// operations is highly dependent on the backend used.
pub trait DirectedWeightedGraph<'g, N, W>
where
    N: 'g + Eq,
    W: 'g,
{
    /// An iterator that iterates over all edges.
    type AllWeightedEdges: Iterator<Item = (&'g N, &'g N, &'g W)>;
    /// An iterator that iterates over all edges, giving mutable access to the
    /// weight.
    type AllWeightedEdgesMut: Iterator<Item = (&'g N, &'g N, &'g mut W)>;

    /// Returns an iterator over all the edges in this graph.
    fn all_edges(&'g self) -> Self::AllWeightedEdges;

    /// Returns an iterator over all the ediges in this graph, with mutable
    /// access to the weight.
    fn all_edges_mut(&'g mut self) -> Self::AllWeightedEdgesMut;

    /// Adds an edge to this graph.
    fn add_edge(&'g mut self, N, N, W) -> Option<(N, N, W)>;

    /// Removes an edge from the graph.
    fn remove_edge(&'g mut self, &N, &N) -> Option<(N, N, W)>;

    /// Returns the amount of edges this graph has.
    ///
    /// # Complexity
    /// The default implementation runs in O(|E|) time.
    fn edge_count(&'g self) -> usize {
        self.all_edges().count()
    }

    /// Checks if a graph has an edge or not.
    ///
    /// # Complexity
    /// The default implementation runs in O(|E|) time.
    fn has_edge(&'g self, source: &N, sink: &N) -> bool {
        self.edge_weight(source, sink).is_some()
    }

    /// Gets a borrow to the weight of the requested edge, if it exist.
    ///
    /// # Complexity
    /// The default implementation runs in O(|E|).
    fn edge_weight(&'g self, source: &N, sink: &N) -> Option<&W> {
        self.all_edges()
            .find(|&(n1, n2, _)| n1 == source && n2 == sink)
            .map(|(_, _, weight)| weight)
    }

    /// Gets a mutable borrow to the weight of the requested edge, if it exist.
    ///
    /// # Complexity
    /// The default implementation runs in O(|E|) time.
    fn edge_weight_mut(&'g mut self, source: &N, sink: &N) -> Option<&mut W> {
        self.all_edges_mut()
            .find(|&(n1, n2, _)| n1 == source && n2 == sink)
            .map(|(_, _, weight)| weight)
    }
}
