use DirectedWeightedGraph;
use collections::Map;
use std::iter;

/// A directed, weighted graph that stores edges and their weights in a map.
///
/// # Examples
///
/// ```
/// use graph_rs::{DirectedMapGraph, DirectedWeightedGraph};
/// use std::collections::HashMap;
///
/// let mut graph: DirectedMapGraph<_> = HashMap::new().into();
/// graph.add_edge(0, 1, 15.0);
/// graph.add_edge(1, 2, 17.5);
///
/// assert_eq!(graph.edge_count(), 2);
/// ```
pub struct DirectedMapGraph<M>(M);

impl<M> From<M> for DirectedMapGraph<M> {
    fn from(map: M) -> Self {
        DirectedMapGraph(map)
    }
}

impl<M> DirectedMapGraph<M> {
    /// Returns the inner `Map` of this graph.
    pub fn unwrap(self) -> M {
        let DirectedMapGraph(graph) = self;
        graph
    }
}

// This implementation requires the key to be cloned.
impl<'m, N, W, M> DirectedWeightedGraph<'m, N, W> for DirectedMapGraph<M>
where
    M: Map<'m, (N, N), W>,
    N: 'm + Eq + Clone,
    W: 'm,
{
    type AllWeightedEdges = iter::Map<M::Iter, fn((&'m (N, N), &'m W)) -> (&'m N, &'m N, &'m W)>;
    type AllWeightedEdgesMut = iter::Map<
        M::IterMut,
        fn((&'m (N, N), &'m mut W)) -> (&'m N, &'m N, &'m mut W),
    >;

    fn all_edges(&'m self) -> Self::AllWeightedEdges {
        fn map_weighted_edge<'m, N, W>(
            (&(ref source, ref sink), weight): (&'m (N, N), &'m W),
        ) -> (&'m N, &'m N, &'m W) {
            (source, sink, weight)
        }

        let &DirectedMapGraph(ref map) = self;

        map.iter().map(map_weighted_edge)

    }

    fn all_edges_mut(&'m mut self) -> Self::AllWeightedEdgesMut {
        fn map_weighted_edge_mut<'m, N, W>(
            (&(ref source, ref sink), &mut ref mut weight): (&'m (N, N), &'m mut W),
        ) -> (&'m N, &'m N, &'m mut W) {
            (source, sink, weight)
        }

        let &mut DirectedMapGraph(ref mut map) = self;

        map.iter_mut().map(map_weighted_edge_mut)
    }

    fn add_edge(&'m mut self, source: N, sink: N, weight: W) -> Option<(N, N, W)> {
        let &mut DirectedMapGraph(ref mut map) = self;

        map.insert((source, sink), weight)
            .map(|((source, sink), value)| (source, sink, value))
    }

    // Clones the key!
    fn remove_edge(&'m mut self, source: &N, sink: &N) -> Option<(N, N, W)> {
        let &mut DirectedMapGraph(ref mut map) = self;
        let key = (source.clone(), sink.clone());

        map.remove(&key)
            .map(|((source, sink), value)| (source, sink, value))
    }

    // Clones the key!
    fn edge_weight(&'m self, source: &N, sink: &N) -> Option<&W> {
        let &DirectedMapGraph(ref map) = self;
        map.get(&(source.clone(), sink.clone()))
    }

    // Clones the key!
    fn edge_weight_mut(&'m mut self, source: &N, sink: &N) -> Option<&mut W> {
        let &mut DirectedMapGraph(ref mut map) = self;
        map.get_mut(&(source.clone(), sink.clone()))
    }
}

#[cfg(test)]
mod tests {
    use {DirectedMapGraph, DirectedWeightedGraph};
    use std::collections::HashMap;

    #[test]
    fn test_add_edge() {
        let mut graph: DirectedMapGraph<_> = HashMap::new().into();

        graph.add_edge(0, 1, 1.0);
        graph.add_edge(1, 1, 2.0);
        graph.add_edge(1, 2, 3.0);

        assert!(graph.has_edge(&0, &1));
        assert!(graph.has_edge(&1, &1));
        assert!(graph.has_edge(&1, &2));
        assert!(!graph.has_edge(&0, &0));
        assert!(!graph.has_edge(&0, &2));
        assert!(!graph.has_edge(&1, &0));
        assert!(!graph.has_edge(&1, &4));

        assert_eq!(graph.edge_weight(&0, &1), Some(&1.0));
        assert_eq!(graph.edge_weight(&1, &1), Some(&2.0));
        assert_eq!(graph.edge_weight(&1, &2), Some(&3.0));

        assert_eq!(graph.all_edges().len(), 3);
    }
}
